FROM openjdk:8
ADD target/back.jar back.jar
EXPOSE 80
ENTRYPOINT ["java", "-jar", "-Dspring.profiles.active=prod", "back.jar"]