package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.RouteDTO;
import com.genapi.back.dto.RowInfoDTO;
import com.genapi.back.dto.TableRequestDTO;
import com.genapi.back.dto.TableResponseDTO;
import com.genapi.back.enums.ResponseTypeEnum;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.listeners.events.DeleteRouteEvent;
import com.genapi.back.service.RoutesService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@RequestMapping("/api/routes")
@RestController
public class RoutesController {

    private final ProjectsController projectsController;
    private final RoutesService routesService;
    private final ApplicationEventPublisher publisher;
    private final SecurityUtility securityUtility;

    @HasRole(RolesEnum.USER)
    @PostMapping("/create")
    public ResponseEntity<RouteDTO> create(@RequestBody RouteDTO routeDTO) throws IOException {
        if(!projectsController.checkProject(routeDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(routesService.create(routeDTO));
    }

    @HasRole(RolesEnum.USER)
    @PutMapping("/update")
    public ResponseEntity<RouteDTO> update(@RequestBody RouteDTO routeDTO) throws IOException {
        if(!projectsController.checkProject(routeDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(routesService.update(routeDTO));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-rows")
    public ResponseEntity<TableResponseDTO> getRows(@RequestBody TableRequestDTO request) {
        return ResponseEntity.ok(routesService.getRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getInfoRows(Long projectId) {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(routesService.getInfoRows(projectId));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/{id}")
    public ResponseEntity<RouteDTO> getById(@PathVariable Long id) {
        RouteDTO routeDTO = routesService.getById(id);
        if(!projectsController.checkProject(routeDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(routeDTO);
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-response-types")
    public ResponseEntity<ResponseTypeEnum[]> getResponseTypes() {
        return ResponseEntity.ok(routesService.getResponseTypes());
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<RouteDTO> delete(@PathVariable Long id) {
        RouteDTO routeDTO = routesService.getById(id);

        if(routeDTO == null) {
            return ResponseEntity.notFound().build();
        } else if(!projectsController.checkProject(routeDTO.getProjectId())) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        publisher.publishEvent(new DeleteRouteEvent(id));
        return ResponseEntity.ok(routeDTO);
    }

}
