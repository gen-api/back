package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.*;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.listeners.events.DeleteUserEvent;
import com.genapi.back.service.UsersService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RequestMapping("/api/users")
@RestController
@AllArgsConstructor
public class UsersController {

    private final UsersService usersService;
    private final SecurityUtility securityUtility;
    private final ApplicationEventPublisher publisher;

    @HasRole(RolesEnum.USER)
    @GetMapping("/current")
    public ResponseEntity<UserDTO> getCurrentUser() {
        return ResponseEntity.ok(usersService.getUserByLoginAndClientId(securityUtility.getUserLogin(), securityUtility.getUserAuthClient()));
    }

    @PostMapping("/create")
    public UserDTO create(@RequestBody UserDetailsDTO userDetailsDTO) {
        return usersService.create(userDetailsDTO);
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/create-for-project-client")
    public ResponseEntity<ProjectClientUserDTO> createForProjectClient(@RequestBody ProjectClientUserDetailsDTO projectClientUserDetailsDTO) {
        return ResponseEntity.ok(usersService.createForProjectClient(projectClientUserDetailsDTO, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-project-client-users/{clientId}")
    public ResponseEntity<List<ProjectClientUserDTO>> getProjectClientUsersByClientId(@PathVariable String clientId) {
        return ResponseEntity.ok(usersService.getProjectClientUsersByClientId(clientId, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-project-client-users-rows")
    public ResponseEntity<TableResponseDTO> getProjectClientUsersRows(@RequestBody TableRequestDTO request) {
        return ResponseEntity.ok(usersService.getProjectClientUsersRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete-project-user/{id}")
    public ResponseEntity<ProjectClientUserDTO> deleteProjectUser(@PathVariable Long id) {
        ProjectClientUserDTO projectClientUserDTO = usersService.getProjectClientUser(id, securityUtility.getUserId());
        if(projectClientUserDTO == null) {
            return ResponseEntity.badRequest().build();
        }
        publisher.publishEvent(new DeleteUserEvent(id));
        return ResponseEntity.ok(projectClientUserDTO);
    }

}
