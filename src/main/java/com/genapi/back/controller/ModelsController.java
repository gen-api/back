package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.ModelDTO;
import com.genapi.back.dto.RowInfoDTO;
import com.genapi.back.dto.TableRequestDTO;
import com.genapi.back.dto.TableResponseDTO;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.listeners.events.DeleteModelEvent;
import com.genapi.back.service.ModelsService;
import com.genapi.back.service.UsersService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@RequestMapping("/api/models")
@RestController
public class ModelsController {

    private final ModelsService modelsService;
    private final ProjectsController projectsController;
    private final UsersService usersService;
    private final ApplicationEventPublisher publisher;
    private final SecurityUtility securityUtility;

    @HasRole(RolesEnum.USER)
    @PostMapping("/create")
    public ResponseEntity<ModelDTO> create(@RequestBody ModelDTO modelDTO) throws IOException {
        if(!projectsController.checkProject(modelDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(modelsService.create(modelDTO));
    }

    @HasRole(RolesEnum.USER)
    @PutMapping("/update")
    public ResponseEntity<ModelDTO> update(@RequestBody ModelDTO modelDTO) throws IOException {
        if(!projectsController.checkProject(modelDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(modelsService.update(modelDTO));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-rows")
    public ResponseEntity<TableResponseDTO> getRows(@RequestBody TableRequestDTO request) {
        return ResponseEntity.ok(modelsService.getRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getInfoRows(@RequestParam Long projectId) {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(modelsService.getInfoRows(projectId));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-all-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getAllInfoRows() {
        return ResponseEntity.ok(modelsService.getAllInfoRows(securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/{id}")
    public ResponseEntity<ModelDTO> getById(@PathVariable Long id) throws IOException {
        ModelDTO result = modelsService.getById(id);
        if(result == null || !projectsController.checkProject(result.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(result);
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ModelDTO> delete(@PathVariable Long id) throws IOException {
        ModelDTO result = modelsService.getById(id);
        if(result == null || !projectsController.checkProject(result.getProjectId())) return ResponseEntity.badRequest().build();
        publisher.publishEvent(new DeleteModelEvent(id));
        return ResponseEntity.ok(result);
    }

}
