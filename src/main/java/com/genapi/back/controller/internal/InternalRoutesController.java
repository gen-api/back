package com.genapi.back.controller.internal;

import com.genapi.back.controller.ProjectsController;
import com.genapi.back.dto.RouteDTO;
import com.genapi.back.entity.RoutesViewEntity;
import com.genapi.back.service.RoutesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RequestMapping("/internal/routes")
@RestController
public class InternalRoutesController {

    private final ProjectsController projectsController;
    private final RoutesService routesService;

    @GetMapping("/view-by-route-and-project")
    public RoutesViewEntity getByRouteAndProjectId(@RequestParam String route, @RequestParam Long projectId) {
//        if(!projectsController.checkProject(projectId)) return null;
        return routesService.getViewByRouteAndProjectId(route, projectId);
    }

}
