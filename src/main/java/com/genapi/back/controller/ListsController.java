package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.ListDTO;
import com.genapi.back.dto.RowInfoDTO;
import com.genapi.back.dto.TableRequestDTO;
import com.genapi.back.dto.TableResponseDTO;
import com.genapi.back.enums.FieldTypeEnum;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.listeners.events.DeleteListEvent;
import com.genapi.back.service.ListsService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@RequestMapping("/api/lists")
@RestController
public class ListsController {

    private final ProjectsController projectsController;
    private final ListsService listsService;
    private final ApplicationEventPublisher publisher;
    private final SecurityUtility securityUtility;

    @HasRole(RolesEnum.USER)
    @PostMapping("/create")
    public ResponseEntity<ListDTO> create(@RequestBody ListDTO ListsDTO) throws IOException {
        if(!projectsController.checkProject(ListsDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(listsService.create(ListsDTO));
    }

    @HasRole(RolesEnum.USER)
    @PutMapping("/update")
    public ResponseEntity<ListDTO> update(@RequestBody ListDTO ListsDTO) throws IOException {
        if(!projectsController.checkProject(ListsDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(listsService.update(ListsDTO));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getInfoRows(@RequestParam Long projectId) throws IOException {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(listsService.getInfoRows(projectId));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-rows")
    public  ResponseEntity<TableResponseDTO> getRows(@RequestBody TableRequestDTO request) throws IOException {
        return ResponseEntity.ok(listsService.getRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/{id}")
    public ResponseEntity<ListDTO> getByid(@PathVariable Long id) throws IOException {
        ListDTO result = listsService.getById(id);
        if(result == null || !projectsController.checkProject(result.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(result);
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/by-model")
    public ResponseEntity<List<RowInfoDTO>> getByModelId(@RequestParam Long modelId, @RequestParam Long projectId) throws IOException {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(listsService.getByModelIdAndProjectId(modelId, projectId));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/by-type")
    public ResponseEntity<List<RowInfoDTO>> getByType(@RequestParam FieldTypeEnum type, @RequestParam Long projectId) throws IOException {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(listsService.getByTypeAndProjectId(type, projectId));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/preview/{id}")
    public ResponseEntity<List<Object>> getPreviewById(@PathVariable Long id) throws IOException {
        ListDTO listDTO = listsService.getById(id);
        if(listDTO == null || !projectsController.checkProject(listDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(listsService.getPreviewById(id));
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ListDTO> delete(@PathVariable Long id) throws IOException {
        ListDTO listDTO = listsService.getById(id);
        if(listDTO == null || !projectsController.checkProject(listDTO.getProjectId())) return ResponseEntity.badRequest().build();
        publisher.publishEvent(new DeleteListEvent(id));
        return ResponseEntity.ok(listDTO);
    }

}
