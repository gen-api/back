package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.ProjectClientDTO;
import com.genapi.back.dto.RowInfoDTO;
import com.genapi.back.dto.TableRequestDTO;
import com.genapi.back.dto.TableResponseDTO;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.jooq.tables.pojos.ClientProject;
import com.genapi.back.listeners.events.DeleteProjectClientEvent;
import com.genapi.back.service.ProjectClientsService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RequestMapping("/api/project-clients")
@RestController
public class ProjectClientsController {

    private final ProjectsController projectsController;
    private final ProjectClientsService projectClientsService;
    private final SecurityUtility securityUtility;
    private final ApplicationEventPublisher publisher;

    @HasRole(RolesEnum.USER)
    @PostMapping("/create")
    public ResponseEntity<ProjectClientDTO> createClient(@RequestParam Long projectId, @RequestParam String password) throws Exception {
        if(!projectsController.checkProject(projectId)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(projectClientsService.createClient(projectId, password));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getInfoRows() {
        return ResponseEntity.ok(projectClientsService.getInfoRows(securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows-by-project")
    public ResponseEntity<List<RowInfoDTO>> getRowsByProject(@RequestParam Long projectId) {
        if(!projectsController.checkProject(projectId)) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(projectClientsService.getInfoRowsByProject(projectId));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-rows")
    public ResponseEntity<TableResponseDTO> getRows(@RequestBody TableRequestDTO request) {
        return ResponseEntity.ok(projectClientsService.getRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete/{clientId}")
    public ResponseEntity delete(@PathVariable String clientId) {
        ClientProject clientProject = projectClientsService.getClientProject(clientId);
        if(clientProject == null || !projectsController.checkProject(clientProject.getProjectId())) {
            return ResponseEntity.badRequest().build();
        }
        publisher.publishEvent(new DeleteProjectClientEvent(clientId));
        return ResponseEntity.ok().build();
    }

}
