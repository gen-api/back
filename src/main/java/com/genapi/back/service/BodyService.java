package com.genapi.back.service;

import com.genapi.back.client.feign.YouApiBodyClient;
import com.genapi.back.constants.RegexConstants;
import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.BodyMapper;
import com.genapi.back.dto.mapper.ModelsMapper;
import com.genapi.back.entity.BodyViewEntity;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.FieldTypeEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.Lists;
import com.genapi.back.jooq.tables.pojos.Body;
import com.genapi.back.jooq.tables.pojos.Models;
import com.genapi.back.repository.*;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.bson.Document;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static com.genapi.back.constants.RegexConstants.*;

@AllArgsConstructor
@Service
public class BodyService {

    private final BodyRepository bodyRepository;
    private final BodyViewRepository bodyViewRepository;
    private final BodyMapper bodyMapper;
    private final ProjectsRepository projectsRepository;
    private final ModelsRepository modelsRepository;
    private final ModelsMapper modelsMapper;
    private final ListsRepository listsRepository;
    private final YouApiBodyClient youApiBodyClient;
    private final JdbcTemplate jdbcTemplate;

    public BodyDTO create(BodyDTO bodyDTO) throws IOException {

        if(bodyDTO.getId() != null) {
            bodyDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return bodyDTO;
        }

        if(bodyRepository.existsByNameAndProjectId(bodyDTO.getName(), bodyDTO.getProjectId())) {
            bodyDTO.addError(ErrorsEnum.MATCH_NAME);
            return bodyDTO;
        }

        ModelDTO modelDTO = modelsMapper.entityToDto(modelsRepository.getOne(bodyDTO.getModelId()));

        validate(bodyDTO, modelDTO);

        return save(bodyDTO, modelDTO);
    }

    public BodyDTO update(BodyDTO bodyDTO) throws IOException {

        if(bodyDTO.getId() == null) {
            bodyDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return bodyDTO;
        }

        if(!bodyRepository.existsByIdAndProjectId(bodyDTO.getId(), bodyDTO.getProjectId())) {
            bodyDTO.addError(ErrorsEnum.NOT_MATCH);
            return bodyDTO;
        }

        ModelDTO modelDTO = modelsMapper.entityToDto(modelsRepository.getOne(bodyDTO.getModelId()));

        validate(bodyDTO, modelDTO);

        return save(bodyDTO, modelDTO);

    }

    public TableResponseDTO getRows(TableRequestDTO request, Long projectOwner) {
        List<String> columns = new ArrayList<String>() {
            {
                add("id");
                add("name");
                add("model_id");
                add("project_id");
                add("model_name");
                add("model");
                add("project_name");
                add("project_owner");
            }
        };
        request.getQuery().put("project_owner", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, projectOwner));
        return TableRequestBuilderUtility.buildRequest(columns, "body_view", request, jdbcTemplate);
    }

    public List<RowInfoDTO> getInfoRows(Long projectId) {
        return jdbcTemplate.query("SELECT id, name FROM body_view WHERE project_id = ?",
                new Object[]{projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public List<BodyDTO> getByModel(Long modelId) throws IOException {
        return bodyMapper.viewToDto(bodyViewRepository.findByModelId(modelId));
    }

    public void delete(Long id) {
        bodyRepository.deleteById(id);
        youApiBodyClient.delete(id);
    }

    public void deleteByProject(Long projectId) {
        List<Long> items = new ArrayList<>();
        bodyRepository.removeAllByProjectId(projectId).forEach(item -> items.add(item.getId()));
        if(items.size() == 0) return;
        youApiBodyClient.delete(items);
    }

    public void deleteByModel(Long modelId) {
        List<Long> items = new ArrayList<>();
        bodyRepository.removeAllByModelId(modelId).forEach(item -> items.add(item.getId()));
        if(items.size() == 0) return;
        youApiBodyClient.delete(items);
    }

    public BodyDTO getById(Long id) throws IOException {
        BodyViewEntity bodyView = bodyViewRepository.getById(id);

        if(bodyView == null) return null;

        BodyDTO bodyDTO = bodyMapper.viewToDto(bodyView);

        bodyDTO.setValue(youApiBodyClient.getByIdForBody(id));

        return bodyDTO;
    }

    /*public List<BodyDTO> getByArrayModelId(Long id) throws IOException {
        List<BodyViewEntity> bodyView = bodyViewRepository.getByArrayModelId(id);

        if(bodyView == null) return null;

        List<BodyDTO> bodyDTOS = bodyMapper.viewToDto(bodyView);

        return bodyDTOS;
    }*/

    public Boolean existsByIdAndProjectId(Long id, Long projectId) {
        return bodyRepository.existsByIdAndProjectId(id, projectId);
    }

    public Document getPreviewById(Long id) {
        return youApiBodyClient.getPreviewById(id);
    }

    private BodyDTO save(BodyDTO bodyDTO, ModelDTO modelDTO) throws IOException {
        if(bodyDTO.errorsIsExist()) return bodyDTO;

        Body body = bodyRepository.save(
                bodyMapper.dtoToEntity(bodyDTO)
        );

        youApiBodyClient.save(body.getId(), bodyDTO.getValue().toJson(), (new ObjectMapper()).writeValueAsString(modelDTO.getModel()));

        BodyDTO result = bodyMapper.viewToDto(bodyViewRepository.getById(body.getId()));

        result.setValue(bodyDTO.getValue());

        return result;
    }

    private BodyDTO validate(BodyDTO bodyDTO, ModelDTO modelDTO) throws IOException {

        if(bodyDTO.getName() == null || bodyDTO.getName().length() == 0) {
            bodyDTO.addError("name", ErrorsEnum.REQUIRED);
        }

        if(bodyDTO.getProjectId() == null) {
            bodyDTO.addError("projectId", ErrorsEnum.REQUIRED);
        }else if (!projectsRepository.existsById(bodyDTO.getProjectId())) {
            bodyDTO.addError("projectId", ErrorsEnum.NOT_MATCH);
        }

        checkModel(bodyDTO, modelDTO);

        return bodyDTO;
    }

    private BodyDTO checkModel(BodyDTO bodyDTO, ModelDTO modelDTO) throws IOException {
        boolean modelIsValid = true;

        if(bodyDTO.getModelId() == null) {
            bodyDTO.addError("modelId", ErrorsEnum.REQUIRED);
            modelIsValid = false;
        } else {
            if(modelDTO == null || !modelDTO.getProjectId().equals(bodyDTO.getProjectId())) {
                bodyDTO.addError("modelId", ErrorsEnum.NOT_MATCH);
                modelIsValid = false;
            }

        }

        if(bodyDTO.getValue() == null) {
            bodyDTO.addError("value", ErrorsEnum.REQUIRED);
        } else if(modelIsValid) {
            Document value = bodyDTO.getValue();
            Object fieldValue;
            int fieldCount = 0;

            for (ModelFieldDTO field: modelDTO.getModel()) {
                fieldValue = value.get(field.getName());

                if(fieldValue == null && field.isRequired()) {
                    bodyDTO.addError("value", ErrorsEnum.REQUIRED_PARAMETER);
                    break;
                } else if(fieldValue != null && !bodyFieldIsValid(fieldValue, field.getType())) {
                    bodyDTO.addError("value", ErrorsEnum.NOT_VALID);
                    break;
                }else if(fieldValue != null) {
                    fieldCount++;
                }

            }

            if(value.size() != fieldCount) {
                bodyDTO.addError("value", ErrorsEnum.NOT_VALID);
            }

        }

        return bodyDTO;
    }

    public boolean bodyFieldIsValid(Object value, FieldTypeEnum type) {
        switch (type) {

            case STRING:
                if(value instanceof String) return true;

            case INTAGER:
                if(value.toString().matches("^\\d+$")) return true;

            case FLOAT:
                if(value.toString().matches("^(-)?\\d+(\\.\\d+)?$")) return true;

            case BOOLEAN:
                if(value.toString().matches("^true|false$")) return true;

            case IMAGE:
                if(value instanceof String && String.valueOf(value).matches(RegexConstants.VALID_FIELD_IMAGE)) return true;

            default:
                return false;

        }
    }

    public boolean bodyFieldIsValid(Object value, String type) {
        if(FieldTypeEnum.contains(type)) return bodyFieldIsValid(value, FieldTypeEnum.valueOf(type));
        if (!(value instanceof Integer) && !(value instanceof Long)) return false;

        Long modelId;

        if (type.matches(VALID_TYPE_MODEL)) {
            modelId = Long.valueOf(type.replaceAll("MODEL|\\(|\\)", ""));
            Optional<Body> body = bodyRepository.findById((long) (int) value);
            if(!body.isPresent()) return false;
            if(!body.get().getModelId().equals(modelId)) return false;
        } else if(
                type.matches(VALID_TYPE_MODEL_LIST) ||
                type.matches(VALID_TYPE_INTAGER_LIST) ||
                type.matches(VALID_TYPE_FLOAT_LIST) ||
                type.matches(VALID_TYPE_STRING_LIST) ||
                type.matches(VALID_TYPE_BOOLEAN_LIST) ||
                type.matches(VALID_TYPE_IMAGE_LIST)
        ) {
            Lists list = listsRepository.getOne((long) (int) value);
            if(list == null) return false;
            if(!list.getType().equals(type.replace("[]", ""))) return false;
        } else {
            return false;
        }

        return true;
    }

}
