package com.genapi.back.service;

import com.genapi.back.entity.UsersViewEntity;
import com.genapi.back.repository.UsersViewRepository;
import com.genapi.back.dto.UserJWT;
import com.genapi.back.utils.SecurityUtility;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UsersViewRepository usersViewRepository;

    @Autowired
    private SecurityUtility securityUtility;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        String clientId = securityUtility.getUserLogin();
        UsersViewEntity userDetailsEntity = usersViewRepository.findFirstByLoginAndClientId(login, clientId);

        if(userDetailsEntity == null)
            throw new UsernameNotFoundException("User " + login + " was not found in the database");

        Collection<GrantedAuthority> grantedAuthoritiesList = new ArrayList<>();
        grantedAuthoritiesList.add(new SimpleGrantedAuthority(userDetailsEntity.getRole().toString()));

        Map<String, Object> additionalInfo = userDetailsEntity.getAdditionalInformation() != null ? Document.parse(userDetailsEntity.getAdditionalInformation()) : new HashMap<>();
        additionalInfo.put("id", userDetailsEntity.getId());
        additionalInfo.put("auth_client", clientId);

        return new UserJWT(userDetailsEntity.getLogin(), userDetailsEntity.getPassword(), grantedAuthoritiesList, additionalInfo);
    }

}
