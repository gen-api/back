package com.genapi.back.service;

import com.genapi.back.client.feign.YouApiBodyClient;
import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.ListsMapper;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.FieldTypeEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.Lists;
import com.genapi.back.repository.ListsRepository;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class ListsService {

    private final ListsRepository listsRepository;
    private final ListsMapper listsMapper;
    private final BodyService bodyService;
    private final YouApiBodyClient youApiBodyClient;
    private final JdbcTemplate jdbcTemplate;

    public ListDTO getById(Long id) throws IOException {
        ListDTO listDTO = listsMapper.entityToDto(listsRepository.getOne(id));
        listDTO.setList(this.youApiBodyClient.getByIdForList(id));
        return listDTO;
    }

    public List<Object> getPreviewById(long id) {
        return youApiBodyClient.getPreviewListById(id);
    }

    public List<RowInfoDTO> getByModelIdAndProjectId(Long modelId, Long projectId) throws IOException {
        return jdbcTemplate.query("SELECT id, name FROM lists WHERE type = CONCAT('MODEL(', ?, ')') AND project_id = ?",
                new Object[]{modelId, projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public void delete(Long id) {
        listsRepository.deleteById(id);
        youApiBodyClient.deleteList(id);
    }

    public void deleteByProject(Long projectId) {
        List<Long> items = new ArrayList<>();
        listsRepository.removeAllByProjectId(projectId).forEach(item -> items.add(item.getId()));
        if(items.size() == 0) return;
        youApiBodyClient.deleteList(items);
    }

    public void deleteByModel(Long modelId) {
        List<Long> items = new ArrayList<>();
        listsRepository.removeAllByTypeLike("MODEL(" + modelId + ")%").forEach(item -> items.add(item.getId()));
        if(items.size() == 0) return;
        youApiBodyClient.deleteList(items);
    }

    public List<RowInfoDTO> getByTypeAndProjectId(FieldTypeEnum type, Long projectId) throws IOException {
        return jdbcTemplate.query("SELECT id, name FROM lists WHERE type = ? AND project_id = ?",
                new Object[]{type, projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public TableResponseDTO getRows(TableRequestDTO request, Long projectOwner) throws IOException {
        List<String> columns = new ArrayList<String>() {
            {
                add("id");
                add("name");
                add("project_id");
                add("type");
                add("project_name");
                add("project_owner");
            }
        };
        request.getQuery().put("project_owner", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, projectOwner));
        return TableRequestBuilderUtility.buildRequest(columns, "lists_view", request, jdbcTemplate);
    }

    public List<RowInfoDTO> getInfoRows(Long projectId) {
        return jdbcTemplate.query("SELECT id, name FROM lists WHERE project_id = ?",
                new Object[]{projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public ListDTO create(ListDTO listDTO) throws IOException {

        if(listDTO.getId() != null) {
            listDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return listDTO;
        }

        if(listsRepository.existsByNameAndProjectId(listDTO.getName(), listDTO.getProjectId())) {
            listDTO.addError(ErrorsEnum.MATCH_NAME);
            return listDTO;
        }

        validate(listDTO);

        return save(listDTO);
    }

    public ListDTO update(ListDTO listDTO) throws IOException {

        if(listDTO.getId() == null) {
            listDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return listDTO;
        }

        if(!listsRepository.existsByIdAndProjectId(listDTO.getId(), listDTO.getProjectId())) {
            listDTO.addError(ErrorsEnum.NOT_MATCH);
            return listDTO;
        }

        validate(listDTO);

        return save(listDTO);

    }

    public ListDTO validate(ListDTO listDTO) {
        if(listDTO.getName() == null || listDTO.getName().length() == 0) {
            listDTO.addError("name", ErrorsEnum.REQUIRED);
        }

        if(listDTO.getType() == null) {
            listDTO.addError("type", ErrorsEnum.REQUIRED);
        }else if(listDTO.getList() == null) {
            listDTO.addError("list", ErrorsEnum.REQUIRED);
        } else {
            for(Object item: listDTO.getList()) {
                if(!bodyService.bodyFieldIsValid(item, listDTO.getType())) {
                    listDTO.addError("list", ErrorsEnum.NOT_VALID);
                    break;
                }
            }
        }

        return listDTO;
    }

    public ListDTO save(ListDTO listDTO) throws IOException {
        if(listDTO.errorsIsExist()) return listDTO;
        Lists lists = listsRepository.save(listsMapper.dtoToEntity(listDTO));
        youApiBodyClient.saveList(lists.getId(), (new ObjectMapper()).writeValueAsString(listDTO.getList()), listDTO.getType());
        ListDTO result = listsMapper.entityToDto(lists);
        result.setList(listDTO.getList());
        return result;
    }

}
