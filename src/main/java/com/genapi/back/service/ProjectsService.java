package com.genapi.back.service;

import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.ProjectsMapper;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.Projects;
import com.genapi.back.repository.ProjectsRepository;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@AllArgsConstructor
@Service
public class ProjectsService {

    private final ProjectsRepository projectsRepository;
    private final ProjectsMapper projectsMapper;
    private final JdbcTemplate jdbcTemplate;

    public ProjectDTO create(ProjectDTO projectDTO) {

        if(projectDTO.getId() != null) {
            projectDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return projectDTO;
        }

        if(projectsRepository.existsByNameAndUserId( projectDTO.getName(), projectDTO.getUserId() )) {
            projectDTO.addError(ErrorsEnum.MATCH_NAME);
            return projectDTO;
        }

        validate(projectDTO);

        return save(projectDTO);
    }

    public ProjectDTO update(ProjectDTO projectDTO) {

        validate(projectDTO);

        if(projectDTO.getId() == null) {
            projectDTO.addError(ErrorsEnum.NOT_VALID_BODY);
        }

        if(!projectsRepository.existsByIdAndUserId( projectDTO.getId(), projectDTO.getUserId() )) {
            projectDTO.addError(ErrorsEnum.NOT_MATCH);
        }

        return save(projectDTO);
    }

    public TableResponseDTO getRows(TableRequestDTO request, Long userId) {
        request.getQuery().put("user_id", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, userId));
        return TableRequestBuilderUtility.buildRequest("projects", request, jdbcTemplate);
    }

    public List<RowInfoDTO> getInfoRows(Long userId) {
        return jdbcTemplate.query("SELECT id, name FROM projects WHERE user_id = ?",
                new Object[]{userId},
                new RowMapper<RowInfoDTO>() {
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public List<RowInfoDTO> getInfoRowsByModel(Long modelId, Long userId) {
        return jdbcTemplate.query("SELECT p.id, p.name FROM projects p " +
                                    "LEFT JOIN models m ON m.project_id = p.id " +
                                    "WHERE p.user_id = ? AND m.id = ? " +
                                    "GROUP BY p.id",
                new Object[]{userId, modelId},
                new RowMapper<RowInfoDTO>() {
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public ProjectDTO getById(Long id) {
        return projectsMapper.entityToDto(
                projectsRepository.getOne(id)
        );
    }

    public void delete(Long id) {
        projectsRepository.deleteById(id);
    }

    private ProjectDTO validate(ProjectDTO projectDTO) {
        return projectDTO;
    }

    private ProjectDTO save(ProjectDTO projectDTO) {
        if(projectDTO.errorsIsExist()) return projectDTO;

        Projects project = projectsRepository.save(
                projectsMapper.dtoToEntity(projectDTO)
        );

        return projectsMapper.entityToDto(project);
    }

}
