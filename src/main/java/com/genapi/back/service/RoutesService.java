package com.genapi.back.service;

import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.RoutesMapper;
import com.genapi.back.entity.RoutesViewEntity;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.ResponseTypeEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.Routes;
import com.genapi.back.repository.RoutesRepository;
import com.genapi.back.repository.RoutesViewRepository;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class RoutesService {

    private final RoutesRepository routesRepository;
    private final RoutesMapper routesMapper;
    private final BodyService bodyService;
    private final ListsService listsService;
    private final RoutesViewRepository routesViewRepository;
    private final JdbcTemplate jdbcTemplate;

    public RouteDTO create(RouteDTO routeDTO) throws IOException {
        if(routeDTO.getId() != null) {
            routeDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return routeDTO;
        }

        if(routeDTO.getName() != null && routeDTO.getProjectId() != null) {
            Routes router = routesRepository.findByNameAndProjectId(routeDTO.getName(), routeDTO.getProjectId());

            if(router != null) {
                routeDTO.addError(ErrorsEnum.MATCH_NAME);
                return routeDTO;
            }
        }

        validate(routeDTO);

        return save(routeDTO);
    }

    public RouteDTO update(RouteDTO routeDTO) throws IOException {
        if(routeDTO.getId() == null) {
            routeDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return routeDTO;
        }

        if(routeDTO.getName() != null && routeDTO.getProjectId() != null) {
            Routes router = routesRepository.findByNameAndProjectId(routeDTO.getName(), routeDTO.getProjectId());

            if(router != null && !router.getId().equals(routeDTO.getId())) {
                routeDTO.addError(ErrorsEnum.MATCH_NAME);
                return routeDTO;
            }
            
        }

        validate(routeDTO);

        return save(routeDTO);
    }

    public TableResponseDTO getRows(TableRequestDTO request, Long projectOwner) {
        List<String> columns = new ArrayList<String>() {
            {
                add("id");
                add("name");
                add("route");
                add("project_id");
                add("body_id");
                add("list_id");
                add("response_type");
                add("active");
                add("auth");
                add("project_owner");
                add("project_name");
            }
        };
        request.getQuery().put("project_owner", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, projectOwner));
        return TableRequestBuilderUtility.buildRequest("routes_view", request, jdbcTemplate);
    }

    public List<RowInfoDTO> getInfoRows(Long projectId) {
        return jdbcTemplate.query("SELECT id, name FROM routes WHERE project_id = ?",
                new Object[]{projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public ResponseTypeEnum[] getResponseTypes() {
        return ResponseTypeEnum.values();
    }

    public void delete(Long id) {
        routesRepository.deleteById(id);
    }

    public void deleteByProject(Long projectId) {
        routesRepository.deleteAllByProjectId(projectId);
    }

    public void deleteByBody(Long bodyId) {
        routesRepository.deleteAllByBodyId(bodyId);
    }

    public void deleteByList(Long listId) {
        routesRepository.deleteAllByListId(listId);
    }

    public void deleteByModel(Long modelId) {
        routesRepository.deleteAllByModelId(modelId);
    }

    public RouteDTO getById(Long id) {
        return routesMapper.entityToDto(routesRepository.getById(id));
    }

    public RoutesViewEntity getViewByRouteAndProjectId(String route, Long projectId) {
        return routesViewRepository.getByRouteAndProjectId(route, projectId);
    }

    public RouteDTO validate(RouteDTO routeDTO) throws IOException {
        if(routeDTO.getName() == null) {
            routeDTO.addError("name", ErrorsEnum.REQUIRED);
        }

        if(routeDTO.getProjectId() == null) {
            routeDTO.addError("projectId", ErrorsEnum.REQUIRED);
        } else {

            if(routeDTO.getBodyId() == null && routeDTO.getListId() == null) {
                routeDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            } else if(routeDTO.getBodyId() != null && routeDTO.getListId() != null) {
                routeDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            } else if(routeDTO.getBodyId() != null) {
                if(!bodyService.getById(routeDTO.getBodyId()).getProjectId().equals(routeDTO.getProjectId())) {
                    routeDTO.addError(ErrorsEnum.NOT_VALID_BODY);
                }
            } else if(routeDTO.getListId() != null) {
                if(!listsService.getById(routeDTO.getListId()).getProjectId().equals(routeDTO.getProjectId())) {
                    routeDTO.addError(ErrorsEnum.NOT_VALID_BODY);
                }
            }

        }

        String route = routeDTO.getRoute();

        if(route == null) {
            routeDTO.addError("route", ErrorsEnum.REQUIRED);
        } else {

            if(route.replaceAll("\\/|\\w+|-", "").length() != 0) {
                routeDTO.addError("route", ErrorsEnum.NOT_VALID);
            } else if(route.indexOf("\\\\") != -1 || route.charAt(route.length() - 1) == '/' || route.indexOf("/") != 0) {
                routeDTO.addError("route", ErrorsEnum.NOT_VALID);
            } else if(routeDTO.getProjectId() != null) {
                Routes routes = routesRepository.getByRouteAndProjectId(routeDTO.getRoute().toLowerCase(), routeDTO.getProjectId());
                if(routes != null && (routeDTO.getId() == null || !routes.getId().equals(routeDTO.getId()))) {
                    routeDTO.addError(ErrorsEnum.MATCH_ROUTE);
                }
            }

        }
        return routeDTO;
    }

    public RouteDTO save(RouteDTO routeDTO) {
        if(routeDTO.errorsIsExist()) return routeDTO;
        routeDTO.setRoute(routeDTO.getRoute().toLowerCase());
        return routesMapper.entityToDto(
                routesRepository.save(routesMapper.dtoToEntity(routeDTO))
        );
    }

}
