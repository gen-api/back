package com.genapi.back.dto.mapper;

import com.genapi.back.dto.ProjectClientDTO;
import com.genapi.back.entity.ProjectClientsEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProjectClientMapper {

    ProjectClientDTO entityToDto(ProjectClientsEntity entity);
    List<ProjectClientDTO> entityToDto(List<ProjectClientsEntity> entity);

}
