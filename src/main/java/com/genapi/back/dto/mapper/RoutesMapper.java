package com.genapi.back.dto.mapper;

import com.genapi.back.dto.RouteDTO;
import com.genapi.back.jooq.tables.pojos.Routes;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RoutesMapper {

    Routes dtoToEntity(RouteDTO dto);
    List<Routes> dtoToEntity(List<RouteDTO> dto);

    RouteDTO entityToDto(Routes dto);
    List<RouteDTO> entityToDto(List<Routes> dto);

}
