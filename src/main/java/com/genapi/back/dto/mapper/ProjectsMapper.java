package com.genapi.back.dto.mapper;

import com.genapi.back.dto.ProjectDTO;
import com.genapi.back.jooq.tables.pojos.Projects;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProjectsMapper {

    Projects dtoToEntity(ProjectDTO dto);
    List<Projects> dtoToEntity(List<ProjectDTO> dtos);

    ProjectDTO entityToDto(Projects entity);
    List<ProjectDTO> entityToDto(List<Projects> entity);

}
