package com.genapi.back.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.genapi.back.dto.ModelDTO;
import com.genapi.back.dto.ModelFieldDTO;
import com.genapi.back.entity.ModelsViewEntity;
import com.genapi.back.jooq.tables.pojos.Models;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Mapper(componentModel = "spring", imports = {ModelFieldDTO.class, ObjectMapper.class, Arrays.class})
public interface ModelsMapper {

    @Mapping(target = "model", expression = "java( (new ObjectMapper()).writeValueAsString(dto.getModel()) )")
    Models dtoToEntity(ModelDTO dto) throws IOException;
    List<Models> dtoToEntity(List<ModelDTO> dtos) throws IOException;

    @Mapping(target = "model", expression = "java( Arrays.asList((new ObjectMapper()).readValue(entity.getModel(), ModelFieldDTO[].class)) )")
    ModelDTO entityToDto(Models entity) throws IOException;
    List<ModelDTO> entityToDto(List<Models> entity) throws IOException;

    @Mapping(target = "model", expression = "java( Arrays.asList((new ObjectMapper()).readValue(entity.getModel(), ModelFieldDTO[].class)) )")
    ModelDTO viewToDto(ModelsViewEntity entity) throws IOException;
    List<ModelDTO> viewToDto(List<ModelsViewEntity> entity) throws IOException;

}
