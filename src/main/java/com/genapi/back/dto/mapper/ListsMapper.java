package com.genapi.back.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.genapi.back.dto.ListDTO;
import com.genapi.back.jooq.tables.pojos.Lists;
import org.mapstruct.Mapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Mapper(componentModel = "spring", imports = {ObjectMapper.class, Arrays.class})
public interface ListsMapper {

    Lists dtoToEntity(ListDTO dto) throws IOException;
    List<Lists> dtoToEntity(List<ListDTO> dto) throws IOException;

    ListDTO entityToDto(Lists entity) throws IOException;
    List<ListDTO> entityToDto(List<Lists> entity) throws IOException;

}
