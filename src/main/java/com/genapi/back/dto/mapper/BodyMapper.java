package com.genapi.back.dto.mapper;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.genapi.back.dto.BodyDTO;
import com.genapi.back.dto.ModelFieldDTO;
import com.genapi.back.entity.BodyViewEntity;
import com.genapi.back.jooq.tables.pojos.Body;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@Mapper(componentModel = "spring", imports = {ObjectMapper.class, HashMap.class, ModelFieldDTO.class, Arrays.class})
public interface BodyMapper {

//    @Mapping(target = "value", expression = "java( dto.getValue()!=null ? (new ObjectMapper()).writeValueAsString(dto.getValue()) : null )")
    Body dtoToEntity(BodyDTO dto) throws IOException;
    List<Body> dtoToEntity(List<BodyDTO> dtos) throws IOException;

//    @Mapping(target = "value", expression = "java( entity.getValue()!=null ? (new ObjectMapper()).readValue(entity.getValue(), Object.class) : null )")
    BodyDTO entityToDto(Body entity) throws IOException;
    List<BodyDTO> entityToDto(List<Body> entity) throws IOException;

//    @Mapping(target = "value", expression = "java( entity.getValue()!=null ? (new ObjectMapper()).readValue(entity.getValue(), Object.class) : null )")
    @Mapping(target = "model", expression = "java( Arrays.asList((new ObjectMapper()).readValue(entity.getModel(), ModelFieldDTO[].class)) )")
    BodyDTO viewToDto(BodyViewEntity entity) throws IOException;
    List<BodyDTO> viewToDto(List<BodyViewEntity> entity) throws IOException;

}
