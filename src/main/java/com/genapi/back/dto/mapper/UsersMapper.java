package com.genapi.back.dto.mapper;

import com.genapi.back.dto.ProjectClientUserDTO;
import com.genapi.back.dto.ProjectClientUserDetailsDTO;
import com.genapi.back.dto.UserDTO;
import com.genapi.back.dto.UserDetailsDTO;
import com.genapi.back.entity.ProjectUsersEntity;
import com.genapi.back.entity.UsersViewEntity;
import com.genapi.back.enums.YouAreEnum;
import com.genapi.back.jooq.tables.pojos.Users;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", imports = YouAreEnum.class)
public interface UsersMapper {

    Users dtoToEntity(UserDTO dto);
    List<Users> dtoToEntity(List<UserDTO> dtos);

    UserDetailsDTO viewToDetails(UsersViewEntity view);
    List<UserDetailsDTO> viewToDetails(List<UsersViewEntity> views);

    Users detailsToEntity(UserDetailsDTO details);
    List<Users> detailsToEntity(List<UserDetailsDTO> details);

    UserDTO detailsToDto(UserDetailsDTO details);
    List<UserDTO> detailsToDto(List<UserDetailsDTO> details);

    UserDTO viewToDto(UsersViewEntity view);
    List<UserDTO> viewToDto(List<UsersViewEntity> views);

    ProjectClientUserDTO projectClientUserDetailsToProjectClientUser(ProjectClientUserDetailsDTO dto);
    List<ProjectClientUserDTO> projectClientUserDetailsToProjectClientUser(List<ProjectClientUserDetailsDTO> dtos);

    Users projectClientUserDetailsToEntity(ProjectClientUserDetailsDTO dto);
    List<Users> projectClientUserDetailsToEntity(List<ProjectClientUserDetailsDTO> dtos);

    ProjectClientUserDTO entityToProjectClientUser(Users entity);
    List<ProjectClientUserDTO> entityToProjectClientUser(List<Users> entity);

    ProjectClientUserDTO projectUsersEntityToProjectClientUser(ProjectUsersEntity projectUsersEntity);
    List<ProjectClientUserDTO> projectUsersEntityToProjectClientUser(List<ProjectUsersEntity> projectUsersEntity);

}
