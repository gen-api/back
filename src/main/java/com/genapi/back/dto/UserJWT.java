package com.genapi.back.dto;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Map;

@Data
public class UserJWT extends User{

    private Map<String, Object> additionalInfo;

    public UserJWT(String login, String password, Collection<GrantedAuthority> grantedAuthoritiesList, Map<String, Object> additionalInfo) {
        super(login, password, grantedAuthoritiesList);
        this.additionalInfo = additionalInfo;
    }

}
