package com.genapi.back.dto;

import lombok.Data;

@Data
public class ModelFieldDTO {
    private String name;
    private String type;
    private boolean required;
}
