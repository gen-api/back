package com.genapi.back.dto;

import com.genapi.back.enums.RolesEnum;
import lombok.Data;

@Data
public class UserDTO extends DTO {
    private Long id;
    private String name;
    private String login;
    private String email;
    private RolesEnum role;
}
