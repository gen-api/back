package com.genapi.back.dto;

import lombok.Data;

@Data
public class ProjectClientUserDTO extends DTO {

    private Long id;
    private String login;
    private String clientId;
    private Long projectId;
    private String projectName;

}
