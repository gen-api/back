package com.genapi.back.dto;

import lombok.Data;

@Data
public class RegisterUserDTO extends DTO {
    private String name;
    private String login;
    private String password;
    private String email;
}
