package com.genapi.back.dto;

import lombok.Data;

@Data
public class ProjectClientUserDetailsDTO extends DTO {

    private Long id;
    private String login;
    private String password;
    private String clientId;

}
