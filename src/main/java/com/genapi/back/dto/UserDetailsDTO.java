package com.genapi.back.dto;

import com.genapi.back.enums.RolesEnum;
import com.genapi.back.enums.YouAreEnum;
import lombok.Data;

@Data
public class UserDetailsDTO extends DTO {

    Long id;
    String name;
    String login;
    String email;
    YouAreEnum youAre;
    String companyName;
    String password;
    RolesEnum role;

}
