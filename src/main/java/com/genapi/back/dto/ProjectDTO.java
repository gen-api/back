package com.genapi.back.dto;

import lombok.Data;

@Data
public class ProjectDTO extends DTO {
    private Long id;
    private String name;
    private Long userId;
    private boolean auth;
    private boolean active;
}
