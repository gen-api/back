package com.genapi.back.dto;

import lombok.Data;

import java.util.Map;

@Data
public class TableRequestDTO {

    private Map<String, TableRequestQueryDTO> query;
    private Long limit;
    private Integer ascending;
    private Long page;
    private Integer byColumn;
    private String orderBy;

}
