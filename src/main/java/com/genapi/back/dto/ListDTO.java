package com.genapi.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListDTO extends DTO {
    private Long id;
    private String name;
    private Long projectId;
    private String type;
    private List<Object> list;
}
