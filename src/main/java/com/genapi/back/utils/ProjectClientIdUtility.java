package com.genapi.back.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;

public abstract class ProjectClientIdUtility {

    public static String generate(Long prjectId) {
        Random random = new Random();
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Paris"));
        cal.setTime(new Date());
        return String.format("project-client-%d-%d-%d.%d.%d.%d.%d.%d.%d",
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.HOUR),
                cal.get(Calendar.MINUTE),
                cal.get(Calendar.SECOND),
                cal.get(Calendar.MILLISECOND),
                random.nextInt(256),
                prjectId);
    }

}
