package com.genapi.back.enums;

public enum RolesEnum {

    ADMIN,
    USER,
    PROJECT_USER,
    ROLE_ANONYMOUS;

}
