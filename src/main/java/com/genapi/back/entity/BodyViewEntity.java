package com.genapi.back.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "body_view")
public class BodyViewEntity {

    @Id
    private Long   id;
    private String name;
    private Long   modelId;
    private Long   projectId;
    private String modelName;
    private String model;
    private String projectName;
    private Long projectOwner;

}
