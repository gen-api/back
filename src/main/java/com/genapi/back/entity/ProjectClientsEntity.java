package com.genapi.back.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "project_clients")
public class ProjectClientsEntity {

    @Id
    private String clientId;
    private String clientSecret;
    private Long projectId;
    private String projectName;
    private Long projectOwner;

}
