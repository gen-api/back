package com.genapi.back.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Table(name = "project_users")
@Entity
public class ProjectUsersEntity {

    @Id
    private Long id;
    private String login;
    private String password;
    private Long roleId;
    private String clientId;
    private Long projectId;
    private String projectName;
    private Long projectOwner;

}
