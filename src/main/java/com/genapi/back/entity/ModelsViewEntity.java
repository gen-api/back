package com.genapi.back.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "models_view")
public class ModelsViewEntity {

    @Id
    private Long id;
    private String name;
    private Long projectId;
    private String model;
    private Long bodyCount;
    private Long listCount;
    private Long projectOwner;
    private String projectName;

}
