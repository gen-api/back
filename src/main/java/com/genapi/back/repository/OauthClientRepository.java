package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.OauthClientDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OauthClientRepository extends JpaRepository<OauthClientDetails, String> {
    OauthClientDetails getByClientId(String clientId);
    void deleteAllByClientIdIn(List<String> ids);
}
