package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {
    void deleteAllByClientId(String clientId);
    void deleteAllByIdIn(List<Long> ids);
    Users getByEmail(String email);
}
