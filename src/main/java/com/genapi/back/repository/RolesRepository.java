package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Roles;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RolesRepository extends JpaRepository<Roles, Long> {
    Roles findByName(String name);
}
