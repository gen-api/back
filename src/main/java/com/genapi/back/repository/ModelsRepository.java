package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Models;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ModelsRepository extends JpaRepository<Models, Long> {
    List<Models> findAllByIdInAndProjectId(List<Long> ids, Long projectId);
    List<Models> findAllByProjectId(Long projectId);
    boolean existsByNameAndProjectId(String name, Long projectId);
    Models getByNameAndProjectId(String name, Long projectId);
    boolean existsByIdAndProjectId(Long id, Long projectId);
    void deleteAllByProjectId(Long projectId);
}
