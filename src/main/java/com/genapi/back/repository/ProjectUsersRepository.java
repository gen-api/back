package com.genapi.back.repository;

import com.genapi.back.entity.ProjectUsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectUsersRepository extends JpaRepository<ProjectUsersEntity, Long> {
    boolean existsByClientIdAndLogin(String clientId, String login);
    List<ProjectUsersEntity> getAllByClientIdAndProjectOwner(String clientId, Long projectOwner);
    List<ProjectUsersEntity> getAllByProjectOwner(Long projectOwner);
    ProjectUsersEntity findByIdAndProjectOwner(Long id, Long projectOwner);
    List<ProjectUsersEntity> getAllByProjectId(Long projectId);
}
