package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Lists;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListsRepository extends JpaRepository<Lists, Long> {
    Boolean existsByNameAndProjectId(String name, Long projectId);
    Boolean existsByIdAndProjectId(Long id, Long projectId);
    Boolean existsByTypeAndProjectId(String type, Long projectId);
    List<Lists> getAllByProjectId(Long projectId);
    List<Lists> getAllByTypeAndProjectId(String type, Long projectId);
    Lists removeById(Long id);
    List<Lists> removeAllByProjectId(Long projectId);
    List<Lists> removeAllByTypeLike(String type);
}
