package com.genapi.back.repository;

import com.genapi.back.entity.ModelsViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ModelsViewRepository extends JpaRepository<ModelsViewEntity, Long> {
    ModelsViewEntity getById(Long id);
    ModelsViewEntity getByNameAndProjectId(String name, Long projectId);
    List<ModelsViewEntity> findAllByProjectId(Long projectId);
}
