package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Body;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BodyRepository extends JpaRepository<Body, Long> {
    boolean existsByNameAndProjectId(String name, Long projectId);
    boolean existsByIdAndProjectId(Long id, Long projectId);
    boolean existsByIdInAndModelIdAndProjectId(Long id, Long modelId, Long projectId);
    Body removeById(Long id);
    List<Body> removeAllByProjectId(Long projectId);
    List<Body> removeAllByModelId(Long modelId);
}
