package com.genapi.back.repository;

import com.genapi.back.entity.ProjectClientsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectClientsRepository extends JpaRepository<ProjectClientsEntity, String> {
    List<ProjectClientsEntity> getAllByProjectIdAndProjectOwner(Long projectId, Long projectOwner);
    List<ProjectClientsEntity> getAllByProjectOwner(Long projectOwner);
    boolean existsByClientIdAndProjectOwner(String clientId, Long projectOwnew);
    ProjectClientsEntity getFirstByClientId(String clientid);
}
