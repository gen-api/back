package com.genapi.back.repository;


import com.genapi.back.entity.RoutesViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoutesViewRepository extends JpaRepository<RoutesViewEntity, Long> {
    RoutesViewEntity getByRouteAndProjectId(String route, Long projectId);
}
