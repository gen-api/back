package com.genapi.back.repository;

import com.genapi.back.entity.UsersViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UsersViewRepository extends JpaRepository<UsersViewEntity, Long> {
    Boolean existsAllByLogin(String login);
    UsersViewEntity findFirstByLoginAndClientId(String login, String clientId);
    List<UsersViewEntity> getAllByClientId(String clientId);
}
