package com.genapi.back.listeners;


import com.genapi.back.listeners.events.*;
import com.genapi.back.service.*;
import lombok.AllArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Component
public class DeleteListener {

    private final ProjectsService projectsService;
    private final RoutesService routesService;
    private final BodyService bodyService;
    private final ListsService listsService;
    private final UsersService usersService;
    private final ModelsService modelsService;
    private final ProjectClientsService projectClientsService;

    @Async
    @EventListener
    public void deleteRoute(DeleteRouteEvent event) {
        routesService.delete(event.getId());
    }

    @Async
    @EventListener
    @Transactional
    public void deleteBody(DeleteBodyEvent event) {
        routesService.deleteByBody(event.getId());
        bodyService.delete(event.getId());
    }

    @Async
    @EventListener
    @Transactional
    public void deleteList(DeleteListEvent event) {
        routesService.deleteByList(event.getId());
        listsService.delete(event.getId());
    }

    @Async
    @EventListener
    public void deleteUser(DeleteUserEvent event) {
        usersService.deleteUser(event.getId());
    }

    @Async
    @EventListener
    @Transactional
    public void deleteProjectClient(DeleteProjectClientEvent event) {
        usersService.deleteAllByClientId(event.getClientId());
        projectClientsService.delete(event.getClientId());
    }

    @Async
    @EventListener
    @Transactional
    public void deleteModel(DeleteModelEvent event) {
        routesService.deleteByModel(event.getId());
        bodyService.deleteByModel(event.getId());
        listsService.deleteByModel(event.getId());
        modelsService.delete(event.getId());
    }

    @Async
    @EventListener
    @Transactional
    public void deleteProject(DeleteProjectEvent event) {
        routesService.deleteByProject(event.getId());
        bodyService.deleteByProject(event.getId());
        listsService.deleteByProject(event.getId());
        modelsService.deleteByProject(event.getId());
        usersService.deleteProjectUserByProject(event.getId());
        projectClientsService.deleteByProject(event.getId());
        projectsService.delete(event.getId());
    }

}
