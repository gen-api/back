package com.genapi.back.listeners.events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DeleteProjectEvent {

    private Long id;

}
