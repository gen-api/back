package com.genapi.back.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.genapi.back.client.feign")
public class FeignConfiguration {

}
