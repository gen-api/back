-- roles
INSERT INTO roles (id, name) VALUES (1, 'ADMIN'), (2, 'USER');

-- users
INSERT INTO users(id, login, password, email, name, role_id) VALUES (1, 'user1', '$2a$04$9o/bRJzFSLiHDIXVqjVLd.r7QqZG37RWxszDH6KSnAzPDZCWAlEH.', 'email@mail.com', 'Витя', 2);

-- projects
INSERT INTO public.projects (id, name, user_login, auth, active) VALUES (1, 'testProject', 'user1', false, true);

-- models
INSERT INTO public.models (id, name, project_id, model) VALUES (1, 'testMode', 1, '[{"name":"name","type":"STRING", "required":false},{"name":"fieldX","type":"STRING", "required":false}]'),
                                                                (2, 'testMode2', 1, '[{"name":"name","type":"STRING", "required":false},{"name":"modelField","type":"MODEL(1)", "required":false}]');

-- body
INSERT INTO public.body (id, name, model_id, project_id) VALUES (1, 'body1', 1, 1), (2, 'body2', 2, 1);
