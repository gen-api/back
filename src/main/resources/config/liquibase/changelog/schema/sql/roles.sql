CREATE TABLE roles
(
  id    BIGSERIAL,
  name  VARCHAR NOT NULL,

  PRIMARY KEY (id)
)