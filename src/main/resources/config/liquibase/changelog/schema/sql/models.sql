CREATE TABLE models
(
  id                BIGSERIAL,
  name              VARCHAR(50) NOT NULL,
  project_id        BIGINT,
  model             TEXT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (name, project_id),
  FOREIGN KEY (project_id) REFERENCES projects (id)
)