ALTER TABLE users ADD client_id VARCHAR(256);

ALTER TABLE users ADD FOREIGN KEY (client_id) REFERENCES oauth_client_details (client_id);

UPDATE users SET client_id = 'default';

ALTER TABLE users ALTER COLUMN client_id SET NOT NULL;

ALTER TABLE projects DROP CONSTRAINT IF EXISTS projects_user_login_fkey;

ALTER TABLE users DROP CONSTRAINT IF EXISTS users_login_key;

ALTER TABLE users ADD UNIQUE (login, client_id);
