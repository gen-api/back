CREATE TABLE body
(
  id          BIGSERIAL,
  name        VARCHAR(50) NOT NULL,
  model_id    BIGINT NOT NULL,
  project_id  BIGINT NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (name, project_id),
  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (model_id) REFERENCES models (id)
)