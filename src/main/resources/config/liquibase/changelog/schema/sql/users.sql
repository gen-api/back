CREATE TABLE users
(
  id          BIGSERIAL,
  login       VARCHAR(50) NOT NULL,
  password    VARCHAR NOT NULL,
  email       VARCHAR NOT NULL,
  name        VARCHAR(50) NOT NULL,
  role_id     BIGINT,

  PRIMARY KEY (id),
  UNIQUE (login),
  UNIQUE (email),
  FOREIGN KEY (role_id) REFERENCES roles (id)
)