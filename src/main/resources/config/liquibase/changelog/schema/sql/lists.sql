CREATE TABLE lists
(
  id            BIGSERIAL,
  name          VARCHAR(50) NOT NULL,
  project_id    BIGINT NOT NULL,
  type          VARCHAR NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (name, project_id),
  FOREIGN KEY (project_id) REFERENCES projects (id)
)