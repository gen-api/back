CREATE TABLE routes
(
  id              BIGSERIAL,
  name            VARCHAR,
  route           VARCHAR NOT NULL,
  project_id      BIGINT,
  body_id         BIGINT,
  list_id         BIGINT,
  response_type   VARCHAR NOT NULL,

  PRIMARY KEY (id),
  UNIQUE (name, project_id),
  UNIQUE (route, project_id),
  FOREIGN KEY (project_id) REFERENCES projects (id),
  FOREIGN KEY (body_id) REFERENCES body (id),
  FOREIGN KEY (list_id) REFERENCES lists (id)

)