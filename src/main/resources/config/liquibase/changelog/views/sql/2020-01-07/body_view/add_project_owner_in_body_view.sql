DROP VIEW IF EXISTS body_view;
CREATE VIEW body_view AS (SELECT b.*,
                                  m.name AS model_name,
                                  m.model,
                                  p.name AS project_name,
                                  p.user_id AS project_owner
                          FROM body AS b
                          LEFT JOIN models AS m ON b.model_id = m.id
                          LEFT JOIN projects AS p ON b.project_id = p.id
                          ORDER BY b.id)