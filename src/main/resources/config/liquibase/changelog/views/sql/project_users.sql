CREATE VIEW project_users AS (SELECT DISTINCT
    u.id,
    u.login,
    u.password,
    u.role_id,
    u.client_id,
    p.id AS project_id,
    p.name AS project_name,
    o.id AS project_owner
      FROM users AS u
        LEFT JOIN roles AS r ON r.id = u.role_id
        LEFT JOIN client_project AS cp ON cp.client_id = u.client_id
        LEFT JOIN projects AS p ON p.id = cp.project_id
        LEFT JOIN users AS o ON o.id = p.user_id
      WHERE r.name = 'PROJECT_USER'
)
