CREATE VIEW lists_view AS (SELECT l.id,
                                  l.name,
                                  l.project_id,

                                  CASE
                                    WHEN l.type LIKE 'MODEL(%)'
                                      THEN (SELECT CONCAT('MODEL(', name, ')')
                                            FROM models m
                                            WHERE m.id =
                                                  REPLACE(
                                                    REPLACE(l.type, 'MODEL(', ''),
                                                    ')',
                                                    ''
                                                  ) :: BIGINT
                                           )
                                    WHEN l.type LIKE 'MODEL(%)[]'
                                      THEN (SELECT CONCAT('MODEL(', name, ')[]')
                                            FROM models m
                                            WHERE m.id =
                                                  REPLACE(
                                                    REPLACE(
                                                      REPLACE(l.type, 'MODEL(', ''),
                                                      ')',
                                                      ''
                                                        ),
                                                    '[]',
                                                    ''
                                                  ) :: BIGINT
                                           )
                                      ELSE l.type
                                  END AS type,

                                  CASE
                                    WHEN l.type LIKE 'MODEL(%)' THEN 'MODEL'
                                    WHEN l.type LIKE 'MODEL(%)[]' THEN 'MODEL[]'
                                    ELSE l.type
                                  END AS correct_type,

                                  p.user_id AS project_owner,
                                  p.name    AS project_name
                           FROM lists l
                                  LEFT JOIN projects p ON l.project_id = p.id)