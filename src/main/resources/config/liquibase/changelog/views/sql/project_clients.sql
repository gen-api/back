CREATE VIEW project_clients AS (SELECT
    c.client_id,
    p.id AS project_id,
    p.name AS project_name,
    p.user_id AS project_owner,
    oc.client_secret
      FROM client_project AS c
        LEFT JOIN projects AS p ON p.id = c.project_id
        LEFT JOIN oauth_client_details AS oc ON oc.client_id = c.client_id
)