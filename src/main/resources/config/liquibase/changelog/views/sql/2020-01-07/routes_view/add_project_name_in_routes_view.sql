DROP VIEW IF EXISTS routes_view;
CREATE VIEW routes_view as(SELECT
  r.*,
  p.active,
  p.auth,
  p.user_id AS project_owner,
  p.name AS project_name
    FROM routes AS r
      LEFT JOIN projects AS p ON r.project_id = p.id
)