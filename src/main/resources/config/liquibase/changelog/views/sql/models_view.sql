CREATE VIEW models_view AS (SELECT m.*,
                          count(b.id) as body_count
                          FROM models m
                          LEFT JOIN body b ON b.model_id = m.id
                          GROUP BY m.id
                          ORDER BY m.id)